from django.conf.urls import patterns, url

from bleacher import views

urlpatterns = patterns('',
    url(r'^list/$', views.list, name='list')
)