from bottle import route, run, template, static_file, request
import os


@route('/static/<filename:path>')
def server_static(filename):
    return static_file(filename, root='./static')


@route('/', method='POST')
def do_upload():
    upload    = request.files.get('upload')
    name, ext = os.path.splitext(upload.filename)
    #if ext not in ('.xls','.xlsx', '.md'):
    #    return template('ohaithar', badext=ext)

    with upload.file as f:
        content = f.read()
    print(content)
    # upload.save('static')  # appends upload.filename automatically
    return template('kthxbye.tpl', kthxbye=True, res=content)


@route('/')
def greet():
    return template('ohaithar.tpl', badext=False)



run(host='localhost', port=8080, debug=True)