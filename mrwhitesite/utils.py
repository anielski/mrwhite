#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import defaultdict

from lxml import etree, html

from model.model import *


roman = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII']
weekdays = [u'poniedziałek', 'wtorek', u'środa', 'czwartek', u'piątek', 'sobota', 'niedziela']
months = ['stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia', 'września',
          u'października', 'listopada', 'grudnia']


def day(k):
    return k[0][0], k[0][1], k[0][2]


def month(k):
    return k[0][0], k[0][1]


# noinspection PyShadowingNames
def date_to_text(y, m, d):
    month = months[m - 1]
    weekday = weekdays[datetime.date(y, m, d).weekday()] + ','
    return ' '.join([weekday, str(d), month, str(y)])


def int_to_roman(i):
    return roman[i - 1]


def regroup_lectures(input_model):
    lects = sorted(input_model.GetLectures(), key=day)

    # group lectures into days
    lects_per_day = defaultdict(list)
    for lect in lects:
        lects_per_day[day(lect)].append(lect)

    #sort days of lectures and split into pairs
    day_pairs = []
    sorted_by_days = sorted(lects_per_day.items(), key=lambda x: x[0])
    for i in range(0, len(sorted_by_days), 2):
        day_pairs.append(tuple(sorted_by_days[i:i + 2]))

    pairs_per_month = defaultdict(list)
    for pair in day_pairs:
        key = month(pair[0])
        pairs_per_month[key].append(pair)

    #sort by month
    pairs_per_month = sorted(pairs_per_month.items(), key=lambda x: x[0])
    return pairs_per_month


def CLASS(*args):
    return {"class": ' '.join(args)}


def build_lecture_tree(lecture, parent, is_odd, input_model):
    tr = etree.SubElement(
        etree.SubElement(
            etree.SubElement(
                etree.SubElement(
                    etree.SubElement(parent, 'tr', CLASS('odd' if is_odd else 'even')),
                    'td', CLASS('inside')),
                'table', CLASS('table-inside-style')),
            'tbody'), 'tr')
    # wlasciwosci zajecia
    etree.SubElement(tr, 'td', CLASS('colGodz')).text = '-'.join(lecture[0][3:5])
    attrs = [('colGrupa', 5), ('colSala', 3), ('colPrzedm', 1), ('colProwadz', 4)]
    class_code = lecture[1][1]
    class_type = lecture[1][0]
    for name, index in attrs:
        attribute = str(lecture[1][index])
        if index == 1:
            attribute = input_model.GetLectureFullname(attribute.upper())
        if index == 4:
            attribute = input_model.GetLectureDefaultLecturer(class_code, class_type)
        etree.SubElement(tr, 'td', CLASS(name)).text = attribute


def build_tree_from_model(pairs_per_month, input_model, with_tabber_nav=False):
    root = etree.Element('div', CLASS('tabberlive' if with_tabber_nav else 'tabber'))

    if with_tabber_nav:
        ul = etree.SubElement(root, 'ul', CLASS('tabbernav'))
        active = True
        for y_m_date, meetings in pairs_per_month:
            m_y = int_to_roman(y_m_date[1]) + ' ' + str(y_m_date[0])
            li = etree.SubElement(ul, 'li', href='javascript:void(null);', title=m_y)
            if active:
                li.set('class', 'tabberactive')
                active = False
            li.text = m_y
        tabber = etree.SubElement(root, 'div', CLASS('tabber'))

    meeting_index = 0
    for y_m_date, meetings in pairs_per_month:
        # TODO: tabber może nie być zainicjowany
        parent = tabber if with_tabber_nav else root
        tab = etree.SubElement(parent, 'div', CLASS('tabbertab'))
        header = int_to_roman(y_m_date[1]) + ' ' + str(y_m_date[0])
        etree.SubElement(tab, 'h2').text = header

        for day_pair in meetings:
            tr = etree.SubElement(
                etree.SubElement(
                    etree.SubElement(tab, 'table', CLASS('table-outside-style'), border='0'),
                    'tbody'),
                'tr')
            etree.SubElement(etree.SubElement(
                etree.SubElement(tr, 'th', CLASS('highlight', 'grey')),
                'div'), 'span').text = "Zjazd " + int_to_roman(meeting_index + 1)

            meeting_index += 1

            # zajecia
            tbody = etree.SubElement(
                etree.SubElement(
                    etree.SubElement(tr, 'td', CLASS('outside')),
                    'table', CLASS('table-style'), style='width:100%;', border='0'),
                'tbody', CLASS('myTableStyle'))
            for y_m_d_date, lects in day_pair:
                etree.SubElement(
                    etree.SubElement(tbody, 'tr', CLASS('header')),
                    'th', CLASS('blue')).text = date_to_text(*y_m_d_date)
                for i, lect in enumerate(lects):
                    build_lecture_tree(lect, tbody, (i + 1) & 1, input_model)
    return root


@turbodynochecked
def generate_html_output(input_model: Model, pretty_print=True, with_tabber_nav=False) -> str:
    pairs_per_month = regroup_lectures(input_model)
    tree = build_tree_from_model(pairs_per_month, input_model, with_tabber_nav=with_tabber_nav)
    output_str = etree.tostring(tree, pretty_print=pretty_print).decode(encoding='utf-8')
    with open("generated.html", "w") as out:
        out.write(output_str)  # ,pretty_print=True)))
    return output_str


def clipboard_to_cells(datas):

    result = []
    for data in datas:
        tree = html.fromstring(data)
        rows = tree.xpath('//table//tr')

        data_result = []
        for row in rows:
            row_result = []
            elems = row.xpath('.//td')
            for elem in elems:
                e_dict = {}
                bg = elem.get('bgcolor')
                e_dict['bgcolor'] = bg
                all_text = elem.xpath('string()').strip()
                if all_text:
                    e_dict['content'] = all_text.strip()
                    e_dict['is_empty'] = False
                else:
                    e_dict['content'] = None
                    e_dict['is_empty'] = True
                row_result.append(e_dict)
            data_result.append(row_result)

        result.append(data_result)
    return result
