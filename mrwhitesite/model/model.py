import datetime
from turbodynochecker import turbodynochecked


class Model:
    def __init__(self):
        self._lectures = {}
        self._hourslimit = {}
        self._codemap = {}  # skrót przedmiotu -> full nazwa przedmiotu
        self._lecturers = {}

########################################################################################################################

    # example: 2013, 11, 1, '08:00', '09:30', 'W', 'TWO'
    @turbodynochecked
    def AddLecture(self,
                   year: int, month: int, day: int,
                   start: str, end: str,
                   class_type: str, code: str,
                   dayofweek=None, room=None, lecturer=None, group=None, spec=None):
        assert len(start) == len(end) == 5, 'hour is not in good format'
        dayofweek = (dayofweek is None and [datetime.date(year, month, day).weekday()] or [dayofweek])[0]
        self._lectures[(year, month, day, start, end, group, spec)] =\
            (class_type, code, dayofweek, room, lecturer, group, spec)

    # example FindLectures(start='8:00', room='307b')
    def FindLectures(self, **kwargs):
        results = []
        kv = ['year', 'month', 'day', 'start', 'end']
        vv = ['type', 'code', 'dayofweek', 'room', 'lecturer', 'group', 'spec']
        kdict = dict(zip(kv, range(len(kv))))
        vdict = dict(zip(vv, range(len(vv))))
        for k, v in self._lectures.items():
            i = 0
            for sk, sv in kwargs.items():
                if (sk in kdict and k[kdict[sk]] == sv) or (sk in vdict and v[vdict[sk]] == sv):
                    i += 1
                else:
                    break
            if i == len(kwargs):
                results.append(k[:-2] + v)
        return results

    def SetLectureHoursLimit(self, code, class_type, limit):
        self._hourslimit[(code, class_type)] = limit

    def AddLectureFullname(self, code, fullname):
        self._codemap[code] = fullname

    def SetLectureDefaultLecturer(self, code, class_type, lecturer):
        self._lecturers[code, class_type] = lecturer

########################################################################################################################

    # list is sorted by lecture start time
    def GetLectures(self):
        return sorted(list(self._lectures.items()), key=lambda x: x[0])

    def GetLectureFullname(self, code):
        return self._codemap.get(code, code)

    def GetLectureDefaultLecturer(self, code, class_type):
        return self._lecturers.get((code, class_type), '')

########################################################################################################################

    def ValideHoursLimit(self):
        return list(filter(lambda x: x[1] != x[2], [(k, v, 0) for k, v in self._hourslimit.items()]))

########################################################################################################################


def LectureToDict(lecture):
    return dict(zip(['type', 'code', 'dayofweek', 'room', 'lecturer', 'group', 'spec'], lecture))


def Updated(record, **kwargs):
    locv = ['year', 'month', 'day', 'start', 'end', 'type', 'code', 'dayofweek', 'room', 'lecturer', 'group', 'spec']
    loc = dict(zip(locv, range(len(locv))))
    record = list(record)
    for k, v in kwargs.items():
        if k in loc:
            record[loc[k]] = v
    return record
