# -*- coding: UTF-8 -*-

import os
from unittest import TestCase
from os import listdir
from os.path import isfile, join
import unittest
from unittest.mock import patch
from model.model import Model
from parsing.utils import ParseException

import utils

import parsing.parser as parsers_A
import parsing.lectures_meta as parsers_B


class test_model(TestCase):

    def test_simple(self):
        m = Model()
        m.AddLecture(2014, 10, 3, '09:30', '11:00', 'W', 'TWO')
        m.AddLecture(2014, 10, 4, '09:30', '11:00', 'L', 'TWO')
        m.AddLecture(2014, 10, 4, '11:30', '13:00', 'L', 'TWO')
        m.AddLecture(2014, 10, 14, '09:30', '11:00', 'L', 'TWO')
        m.AddLecture(2014, 10, 15, '09:30', '11:00', 'L', 'TWO')
        m.AddLecture(2014, 11, 3, '12:30', '14:00', 'L', 'TWO')
        m.AddLecture(2014, 11, 4, '12:30', '14:00', 'L', 'TWO')
        m.AddLecture(2014, 12, 2, '08:00', '09:30', 'L', 'TWO')
        m.AddLecture(2014, 12, 3, '18:00', '19:30', 'L', 'TWO')
        m.SetLectureHoursLimit('TWO', 'L', 30)
        m.SetLectureHoursLimit('TWO', 'W', 30)

        try:
            pre = os.stat("generated.html").st_mtime
        except FileNotFoundError:
            pre = None
        result = utils.generate_html_output(m)
        post = os.stat("generated.html").st_mtime
        self.assertNotEqual(pre, post)
        self.assertGreater(os.stat("generated.html").st_size, 0)
        self.assertNotEqual(result, "")
        with open("generated.html", "r") as f:
            self.assertEqual(result, f.read())


class test_parsers(TestCase):

    def setUp(self):
        pastedtext = []
        dirname = "tests_atfcts"
        for filename in listdir(dirname):
            if isfile(join(dirname, filename)):
                with open(join(dirname, filename), "r", encoding="utf-8") as f:
                    pastedtext.append(f.read())
        [self.sheet_sem1, self.sheet_sem2, self.sheet_nazwy, self.sheet_spis] = utils.clipboard_to_cells(pastedtext)
        self.assertNotEqual(self.sheet_sem1, [])
        self.assertNotEqual(self.sheet_sem2, [])
        self.assertNotEqual(self.sheet_nazwy, [])
        self.assertNotEqual(self.sheet_spis, [])

    def test_sem1_parser(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            parsers_A.ParseSemester(self.sheet_sem1, fakemodel)
            self.assertTrue(fakemodel.AddLecture.called)

    def test_negative_sem1_parser_nazwy(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse3rd(self.sheet_sem1, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_negative_sem1_parser_spis(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse4th(self.sheet_sem1, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_sem2_parser(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            parsers_A.ParseSemester(self.sheet_sem2, fakemodel)
            self.assertTrue(fakemodel.AddLecture.called)

    def test_negative_sem2_parser_nazwy(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse3rd(self.sheet_sem2, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_negative_sem2_parser_spis(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse4th(self.sheet_sem2, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_nazwy_parser(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            parsers_B.Parse3rd(self.sheet_nazwy, fakemodel)
            self.assertTrue(fakemodel.AddLectureFullname.called)

    def test_negative_nazwy_parsers_sem(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_A.ParseSemester(self.sheet_nazwy, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_negative_nazwy_parser_spis(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse4th(self.sheet_nazwy, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    # noinspection PyProtectedMember
    def test_spis_parser(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            parsers_B.Parse4th(self.sheet_spis, fakemodel)
            self.assertTrue(fakemodel.SetLectureDefaultLecturer.called)

    def test_negative_spis_parser_sem(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_A.ParseSemester(self.sheet_spis, fakemodel))
            self.assertRaises(ParseException, lambda: parsers_B.Parse3rd(self.sheet_spis, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    def test_negative_spis_parser_nazwy(self):
        with patch('model.model.Model', spec=Model) as FakeModel:
            fakemodel = FakeModel.return_value
            self.assertRaises(ParseException, lambda: parsers_B.Parse3rd(self.sheet_spis, fakemodel))
            self.assertFalse(fakemodel.AddLecture.called)
            self.assertFalse(fakemodel.AddLectureFullname.called)
            self.assertFalse(fakemodel.SetLectureDefaultLecturer.called)

    # noinspection PyProtectedMember
    def test_nazwy_basic(self):
        model = Model()
        parsers_B.Parse3rd(self.sheet_nazwy, model)
        self.assertNotEqual(model._codemap, {})
        self.assertNotEqual(model.GetLectureFullname("JSCRIPT"), "")

    # noinspection PyProtectedMember
    def test_spis_basic(self):
        model = Model()
        parsers_B.Parse4th(self.sheet_spis, model)
        self.assertNotEqual(model._lecturers, {})


if __name__ == "__main__":
    unittest.main()