import re
import itertools

from model.model import Model
from parsing.utils import ParseException
from turbodynochecker import turbodynochecked, t_any, t_list, t_dict


# noinspection PyPep8Naming

@turbodynochecked
def FindCellByContent(rows: t_list(t_list(t_dict(t_any, t_any))), value):
    for row in rows:
        for cell in row:
            if cell['content'] == value:
                return cell
    return None


def FindCellCoordByContent(rows, value, from_row=0, to_row=-1):
    if to_row == -1:
        to_row = len(rows)
    for row in range(from_row, to_row):
        for cell in range(len(rows[row])):
            if rows[row][cell]['content'] == value:
                return row, cell
    return None


def FindCellCoordByReContent(rows, pattern, row_range=(0, -1)):
    from_row = row_range[0]
    to_row = row_range[1]
    if to_row == -1:
        to_row = len(rows)
    for row in range(from_row, to_row):
        for col in range(len(rows[row])):
            if rows[row][col]['content'] and re.search(pattern, rows[row][col]['content']):
                return row, col
    return None


def ParseLecturesBlock(rows, model, start, colors, prev_spec):
    hour_pattern = r'(\d{1,2}:\d{2})-(\d{1,2}:\d{2})'
    #date_pattern = r'((\d{1,2})\.(\d{1,2}))'
    date_pattern = r'(\d{4})\-(\d{2})\-(\d{2})'
    room_pattern = r'\d{1,2}\.\d{1,2}[a-z]?'
    #lecture_pattern = r'[A-Z\_]+'
    lecture_pattern = r'''
                       ([a-zA-Z_-]+)           # trivia: nazwa przedmiotu
                       (                        # capture group komentarza
                         (                       # łap zero lub jeden
                           \s                     # biały znak
                           \(                     # nawias
                           [a-zA-Z. 0-9]+         # treść komentarza
                           \)                     # nawias
                         )?
                       )
                       '''
    re_lecture = re.compile(lecture_pattern, re.VERBOSE)

    spec_pattern = r'[\w\s]+'

    spec_code = prev_spec
    if start[0] - 1 >= 0:
        upper_cell = rows[start[0] - 1][start[1]]
        upper_content = upper_cell['content']
        if upper_content:
            match = re.search(spec_pattern, upper_content)
            if match:
                spec_code = match.group().strip()

    # wyciaganie wymiarow bloku
    dates_num = 0
    for col in range(1, len(rows[start[0]])):
        if rows[start[0]][col]['content'] and re.search(date_pattern, rows[start[0]][col]['content']):
            dates_num += 1
        else:
            break
    hours_num = 0
    while len(rows) > start[0] + hours_num + 1 and rows[start[0] + hours_num + 1][start[1]]['content'] and re.search(
            hour_pattern, rows[start[0] + hours_num + 1][start[1]]['content']):
        hours_num += 1

    block_str = str(start[0] + 1) + ':' + chr(ord('A') + start[1])
    if dates_num == 0:
        raise ParseException(u'[%s] block lacks dates (or dates in wrong format)' % block_str)
    if hours_num == 0:
        raise ParseException(u'[%s] block lacks hours (or hours in wrong format)' % block_str)

    # iteracja po danych w bloku
    for pair in itertools.product(range(0, hours_num, 2), range(dates_num)):
        cell_coords = (start[0] + pair[0] + 1, start[1] + pair[1] + 1)
        cell_str = str(cell_coords[0] + 1) + ':' + chr(ord('A') + cell_coords[1])

        cell = rows[cell_coords[0]][cell_coords[1]]
        cell_below = rows[cell_coords[0] + 1][cell_coords[1]]

        date_cell = rows[start[0]][start[1] + pair[1] + 1]
        time_cell = rows[start[0] + pair[0] + 1][start[1]]
        time_cell2 = rows[start[0] + pair[0] + 2][start[1]]

        datematch = re.match(date_pattern, date_cell['content'])
        hoursmatch = re.match(hour_pattern, time_cell['content'])
        hoursmatch2 = time_cell2['content'] and re.match(hour_pattern, time_cell2['content']) or None

        if datematch and hoursmatch:
            day = int(datematch.group(3))
            month = int(datematch.group(2))
            year = int(datematch.group(1))
            hours_from = hoursmatch.group(1)
            hours_to = hoursmatch.group(2)

            if hoursmatch2:
                hours_to = hoursmatch2.group(2)

            room0 = None
            room1 = None

            ltype = None
            if cell['bgcolor'] == colors[0]:
                ltype = 'W'
            elif cell['bgcolor'] == colors[1]:
                ltype = 'L'

            if cell['content']:
                roommatch = re.search(room_pattern, cell['content'])
                if roommatch:
                    room0 = roommatch.group()

            if cell_below['content']:
                roommatch = re.search(room_pattern, cell_below['content'])
                if roommatch:
                    room1 = roommatch.group()

            if cell['content']:
                lecturematch = re_lecture.search(cell['content'])
                if not lecturematch:
                    raise ParseException(u'[%s] unrecognised cell content' % cell_str)
                lecture_code = lecturematch and lecturematch.group() or None
                lecture_code_below = None

                if ltype is None:
                    raise ParseException(u'[%s] invalid cell color' % cell_str)

                if cell_below['content']:
                    lecturematch_below = re_lecture.search(cell_below['content'])
                    lecture_code_below = lecturematch_below and lecturematch_below.group() or None

                if room0 is None and lecture_code:
                    # add to both groups
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, None, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room1, None, 1,
                                     spec_code)
                elif lecture_code and lecture_code_below:
                # add to both groups
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room0, None, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code_below, None, room1, None, 1,
                                     spec_code)
                elif lecture_code and not cell_below['content']:
                    # add separate
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room0, None, 0,
                                     spec_code)
                    model.AddLecture(year, month, day, hours_from, hours_to, ltype, lecture_code, None, room0, None, 1,
                                     spec_code)
                else:
                    raise ParseException(u'[%s] unrecognised case' % cell_str)

                    # przypadki:
                    # 1) komórka wyżej: nazwa przedmiotu, komórka niżej: numer sali (lub pusta) -> obie grupy maja
                    # przypisany przedmiot z sala (jesli obecna)
                    # 2) komórka wyżej: przedmiot + sala, komórka niżej: przedmiot + sala -> grupa 1. ma przypisany
                    # przedmiot z komórki wyżej, grupa druga z tej niżej
                    # 3) komórka wyżej: przedmiot (+ sala), komórka niżej: pusta -> obie grupy maja przypisany przedmiot
                    # z sala
    return spec_code

# X 1) nie ma bloków
# 2) w obrębie bloku złe daty/złe godziny/zły dzień tygodnia
# X 3) czy łapie jakiś przypadek
# X 4) brak legendy
# X 5) kolory!
#
# pytanie: jak z tymi godzinami!


# @turbodynochecked
def ParseSemester(rows: t_list(t_list(t_dict(t_any, t_any))),
                  model: Model):
    try:
        lecture_color = FindCellByContent(rows, u'Wykład')['bgcolor']
        lab_color = FindCellByContent(rows, u'Lab.')['bgcolor']
    except:
        raise ParseException(u'legend missing')

    block_pattern = r'Pn|Wt|Śr|Cz|Pt|Sb|Nd|Legenda'

    start = (0, -1)
    spec = None
    blocks = 0
    while True:
        start = FindCellCoordByReContent(rows, block_pattern, (start[0] + 1, -1))
        if start is None:
            break
        if rows[start[0]][start[1]]['content'] != 'Legenda':
            spec = ParseLecturesBlock(rows, model, start, [lecture_color, lab_color], spec)
            blocks += 1

    if blocks == 0:
        raise ParseException(u'no blocks detected')

    for m in model.GetLectures():
        print (m)
