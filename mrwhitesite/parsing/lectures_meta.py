import re
from model.model import Model
from parsing.utils import ParseException
from turbodynochecker import t_list, t_dict, t_any, turbodynochecked


@turbodynochecked
def Parse3rd(input_data: t_list(t_list(t_dict(t_any, t_any))), model: Model):
    if input_data[0][0]["content"] != "Skrót" or input_data[0][1]["content"] != "Pełna nazwa":
        raise ParseException
    for rowid, row in enumerate(input_data[1:]):
        if row[0]["is_empty"]:
            raise ParseException
        model.AddLectureFullname(row[0]["content"], row[1]["content"])


@turbodynochecked
def Parse4th(input_data: t_list(t_list(t_dict(t_any, t_any))), model: Model):
    re_semestry = re.compile(r"""
                                [IVX]+     # numer semestru
                                \s+
                                semestr
                             """,
                             re.VERBOSE)

    found = False
    try:
        rowid = 0
        while input_data[rowid][0]["is_empty"]:
            rowid += 1
        while True:
            if (not re_semestry.match(input_data[rowid][0]["content"])
                    or input_data[rowid + 1][0]["content"] != "Skrót"
                    or input_data[rowid + 1][4]["content"] != "Prowadzący"):
                raise ParseException

            rowid += 2

            while not input_data[rowid][0]["is_empty"]:
                found = True
                model.SetLectureDefaultLecturer(input_data[rowid][0]["content"],
                                                input_data[rowid][5]["content"],
                                                input_data[rowid][4]["content"])
                rowid += 1

            while input_data[rowid][0]["is_empty"]:
                rowid += 1
    except IndexError:
        if not found:
            raise ParseException