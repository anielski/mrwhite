import re


class ParseException(Exception):
    pass


def safe_match(value, pattern):
    match = re.match(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match


def safe_search(value, pattern):
    match = re.search(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match


def safe_match(value, pattern):
    match = re.match(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match

def safe_search(value, pattern):
    match = re.search(value, pattern)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern)
    return match

def safe_match2(value, pattern):
    match = pattern.match(value)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern.pattern)
    return match

def safe_search2(value, pattern):
    match = pattern.search(value)
    if not match:
        raise ParseException(str(value) + ' does not match pattern: ' + pattern.pattern)
    return match
