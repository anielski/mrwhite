Projekt MrWhite
===============

Termin zajęć: czwartek, godzina 8:00


Konfiguracja środowiska - bottle.py
===================================

Brak. Uruchomienie:

    python3.3 site.py  # Python 3.3.2


Konfiguracja środowiska - Django
================================

    virtualenv --python=$(which python3.3) py3env-django  # Python 3.3.2
    . ./py3env-django/bin/activate
    pip install Django  # Django 1.6.1
